<?php

/**
 * @file
 * Template overrides and (pre-)process hooks.
 */

/**
 * Implements hook_html_head_alter().
 */
function icck_base_theme_html_head_alter(&$head) {
  // Simplify the meta tag for character encoding.
  $head['system_meta_content_type']['#attributes'] = array('charset' => str_replace('text/html; charset=', '', $head['system_meta_content_type']['#attributes']['content']));
}